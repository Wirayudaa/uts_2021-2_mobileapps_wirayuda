import React, { useState, useEffect } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Ionicons';
import RNBootSplash from "react-native-bootsplash";
import { firebase } from './firebase/config';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

import LoginScreen from './screens/LoginScreen';
import StartScreen from './screens/StartScreen';
import SignUpScreen from './screens/SignUpScreen';
import ChatScreen from './screens/ChatScreen';
import ProfileScreen from './screens/ProfileScreen';
import LoadingScreen from './screens/LoadingScreen';

const Stack = createStackNavigator();

function App() {
  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState();

  function onAuthStateChanged(user) {
    setUser(user);
    if (initializing) setInitializing(false);
  }

  useEffect(() => {
    const subscriber = firebase.auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber;
  }, []);

  if (initializing) return null;

  return (
    <NavigationContainer onReady={() => RNBootSplash.hide()}>
      <Stack.Navigator>
        { user ?
          <>
            <Stack.Screen name="Chat" component={ChatScreen} options={({ navigation }) => ({
              title: 'STEM Prasetiya Mulya',
              headerRight: () => (
                <TouchableOpacity style={{marginRight: 20}} onPress={() => navigation.navigate('Profile')}>
                  <Icon name="md-person-circle" size={35} color="#000" style={{paddingHorizontal: 5}}/>
                </TouchableOpacity>
              ),
            })}/>
            <Stack.Screen name="Profile" component={ProfileScreen} />
          </>
          
        :
          <>
            <Stack.Screen name="Start" component={StartScreen} options={{
              headerShown: false,
            }}/> 
            <Stack.Screen name="Login" component={LoginScreen} />
            <Stack.Screen name="Sign Up" component={SignUpScreen} />
            <Stack.Screen name="Loading" component={LoadingScreen} />
          </>
        }
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;