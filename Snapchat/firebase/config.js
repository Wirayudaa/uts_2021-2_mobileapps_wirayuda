import * as firebase from 'firebase';
import '@react-native-firebase/auth';
import '@react-native-firebase/firestore';

const firebaseConfig = {
    apiKey: "AIzaSyDv6PhLuoJFbQFzjQ9OyoEFFgW_OYuYpNg",
    authDomain: "snapchat-wirayuda.firebaseapp.com",
    projectId: "snapchat-wirayuda",
    storageBucket: "snapchat-wirayuda.appspot.com",
    messagingSenderId: "759781824753",
    appId: "1:759781824753:web:4167fb9464849d52b6b150",
    measurementId: "G-FE8Y5B7ENE"
};

if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
}

export { firebase };