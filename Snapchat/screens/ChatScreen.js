import React, { useEffect, useState, useRef } from 'react';
import { FlatList, View, Text, TouchableOpacity, Image } from 'react-native';
import { GiftedChat, Send } from 'react-native-gifted-chat'
import { Dialogflow_V2 } from "react-native-dialogflow";
import emojiUtils from 'emoji-utils';
import Icon from 'react-native-vector-icons/FontAwesome';

import SnapchatMessage from '../components/SnapchatMessage';
import { dialogflowConfig } from '../dialogflow/config';

export default class ChatScreen extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        data: [],
        name: '',
        messages: [
          {
            _id: 1,
            text: 'Type "!help" to view all the commands',
            
            createdAt: new Date(),
            user: {
              _id: 2,
              name: 'STEM PRASETIYA MULYA',
            },
          },
          {
            _id: 3,
            text: 'Hi, I am Prasetiya Mulya Chat Bot',
            
            createdAt: new Date(),
            user: {
              _id: 2,
              name: 'STEM PRASETIYA MULYA',
            },
          },
        ],
      }
    }
  
    componentDidMount() {
      fetch('https://kuliahstem.prasetiyamulya.ac.id/web-api/newkuliah/')
      .then((response) => response.json())
      .then((json) => {
        this.setState({ data: json });
      })
      .catch((error) => console.log(error))
      .finally(() => {
        this.setState({ isLoading: false });
      });

      Dialogflow_V2.setConfiguration(
        dialogflowConfig.client_email,
        dialogflowConfig.private_key,
        Dialogflow_V2.LANG_ENGLISH_US,
        dialogflowConfig.project_id
      );
    }

    renderSend(props) {
      return (
          <Send {...props}>
              <View style={{marginRight: 10, marginBottom: 5}}>
                  <Icon name="send" size={25} color="#000" resizeMode={'center'}/>
              </View>
          </Send>
      );
    }

    handleGoogleResponse(result) {
      let text = result.queryResult.fulfillmentMessages[0].text.text[0];

      this.sendBotResponse(text);
    }

    sendBotResponse(text) {;
      let msg;
      let filtered_data = [];
      let datadosen = [];
      const { data } = this.state

      for (var i = 0; i < data.length; i++) {
        datadosen.push(data[i].faculty_name.split(' ').slice(1).join(' ').toLowerCase())
      }
  
      var filterdatadosen = [];
  
      var filterdatadosen = datadosen.filter(function (elem, pos) {
        return datadosen.indexOf(elem) == pos;
      });
  
      var filterdatadosen2 = filterdatadosen

      for (let i = 0; i < data.length; i++){
        if(data[i].faculty_name.toLowerCase().includes(text)){
          filtered_data.push(data[i])
        }
      }

      let datalist = [];
      var i;
      for(i = 0; i < filtered_data.length; i++){
        datalist.push(filtered_data[i].event_name + '\n' + filtered_data[i].day + ' | ' + filtered_data[i].start_time + ' - ' + filtered_data[i].end_time);
      }

      var filtered_datalist = datalist.filter(function (elem, pos) {
        return datalist.indexOf(elem) == pos;
      });
      
      var finaldata = ""
      var x;
      for(x = 0; x < filtered_datalist.length; x++){
        if(x == filtered_datalist.length - 1){
          finaldata += filtered_datalist[x]
        }else {
          finaldata += filtered_datalist[x] + '\n\n'
        };
      }

      if (filterdatadosen2.includes(text)) {
        msg = {
          _id: this.state.messages.length + 1,
          text: finaldata,
          createdAt: new Date(),
          user: {
              _id: 2,
              name: 'STEM PRASETIYA MULYA',
            },
        }
      }else if (text == "schedule"){
        msg = {
          _id: this.state.messages.length + 1,
          text: 'Type "jadwal (nama dosen)" to look for schedule',
          createdAt: new Date(),
          user: {
              _id: 2,
              name: 'PRASETIYA MULYA',
            },
        }
      }else if (text == "hello"){
        msg = {
          _id: this.state.messages.length + 1,
          text: 'Hi! How are you doing?',
          createdAt: new Date(),
          user: {
              _id: 2,
              name: 'STEM PRASETIYA MULYA',
            },
        }
      }else{
        msg = {
          _id: this.state.messages.length + 1,
          text: 'Nama dosen tidak ditemukan.',
          createdAt: new Date(),
          user: {
              _id: 2,
              name: 'PRASETIYA MULYA',
            },
        }
      }

      this.setState(previousState => ({
        messages: GiftedChat.append(previousState.messages, [msg]),
      }))
    }
  
    onSend(messages = []) {
      this.setState(previousState => ({
        messages: GiftedChat.append(previousState.messages, messages),
      }))

      let message = messages[0].text;

      Dialogflow_V2.requestQuery(
        message,
        (result) => this.handleGoogleResponse(result),
        (error) => console.log('Error')
      )
    }


    renderMessage(props) {
      const {
        currentMessage: { text: currText },
      } = props
  
      let messageTextStyle
  
      if (currText && emojiUtils.isPureEmojiString(currText)) {
        messageTextStyle = {
          fontSize: 28,
          lineHeight: Platform.OS === 'android' ? 34 : 30,
        }
      }
  
      return <SnapchatMessage {...props} messageTextStyle={messageTextStyle} />
    }
  
    render() {
      return (
          <GiftedChat
            messages={this.state.messages}
            onSend={messages => this.onSend(messages)}
            onQuickReply={quickReplies => this.onQuickReply(quickReplies)}
            user={{
              _id: 1,
            }}
            renderMessage={this.renderMessage}
            renderSend={this.renderSend}
            alwaysShowSend
            textInputStyle={{color: '#000'}}
          />
      )
    }
  }