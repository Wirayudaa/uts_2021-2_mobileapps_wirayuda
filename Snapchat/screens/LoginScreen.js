import React, { useState } from 'react';
import { View, Text, TextInput, TouchableOpacity, ActivityIndicator, SafeAreaView } from 'react-native';
import { Button } from 'react-native-paper';
import styles from '../styles/styles';
import { firebase } from '../firebase/config';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

import LoadingScreen from './LoadingScreen';

export default function LoginScreen() {
    const [Email, setEmail] = useState('')
    const [Password, setPassword] = useState('')
    const [Loading, setLoading] = useState(false)

    if(Loading) {
        return(
            <LoadingScreen />
        )
    }

    const userLogin = async () => {
        setLoading(true)
        if( !Email || !Password ) {
            alert("Please fill all the fields")
        }
        try {
            const result = await firebase.auth().signInWithEmailAndPassword(Email, Password)
        }
        catch(err) {
            alert('Invalid Email or Password')
        }
        setLoading(false)
    }

    return(
        <SafeAreaView style={styles.container}>
            <Text style={{fontSize: 25, marginBottom: 10}}>Log In</Text>
            <Text style={{color: '#0EADFF', alignSelf: 'flex-start', marginLeft: 30}}>USERNAME OR EMAIL</Text>
            <TextInput 
                style = {styles.input}
                value = {Email}
                onChangeText = {(text) => setEmail(text)}
            />
            <Text style={{color: '#0EADFF', alignSelf: 'flex-start', marginLeft: 30}}>PASSWORD</Text>
            <TextInput 
                style = {styles.input}
                value = {Password}
                onChangeText = {(text) => setPassword(text)}
                secureTextEntry = {true}
            />
            <Text style={{color: '#0EADFF', marginTop: 30}}>Forgot your password?</Text>
            <Button 
                mode="contained"
                style = {{width: 130, height: 45, borderRadius: 50, top: '15%'}}
                disabled = { Email, Password ? false:true }
                onPress = {() => userLogin()}
            >
                <Text style={{textAlign: 'center', fontWeight: 'bold', fontSize: 20, marginVertical: 8, color: '#FFFFFF'}}>Login</Text>
            </Button>
        </SafeAreaView>
    );
}