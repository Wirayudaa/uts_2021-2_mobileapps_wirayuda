import React, { useState } from 'react';
import { View, Text, SafeAreaView, TouchableOpacity } from 'react-native';
import { Button } from 'react-native-paper';
import styles from '../styles/styles';
import Icon from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Feather from 'react-native-vector-icons/Feather';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { firebase } from '../firebase/config';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

import LoadingScreen from './LoadingScreen';

export default function StartScreen({navigation}) {
    const [Loading, setLoading] = useState(false)

    if(Loading) {
        return(
            <LoadingScreen />
        )
    }

    const userLogout = async () => {
        setLoading(true)
        try {
            const result = await firebase.auth().signOut();
        }
        catch(err) {
            alert('Something went wrong. Please try again.')
        }
        setLoading(false)
    }

    return(
        <SafeAreaView style={styles.container}>
            <View style={{flex: 1, alignItems: 'center', top: '5%'}}>
                <View style={{borderWidth: 1, borderRadius: 10, backgroundColor: '#FFFC00'}}>
                    <Icon name="md-person-circle" size={100} color="#000" style={{paddingHorizontal: 5}}/>
                </View>
                <Text style={{marginTop: 20, fontWeight: 'bold', fontSize: 25}}>User</Text>
                <View style={{ width: 350, flexDirection: 'row', paddingVertical: 5, borderRadius: 10, marginTop: 10, borderWidth: 0.2, justifyContent: 'space-between' }}>
                    <View style={{flexDirection: 'row', marginLeft: 10}}>
                        <AntDesign name="contacts" size={30} color="#000" style={{ marginTop: 5 }}/>
                        <TouchableOpacity style={{ marginLeft: 20 }}>
                            <Text style={{fontSize: 18}}>Find friends on Snapchat</Text>
                            <Text style={{color: '#D3D3D3'}}>Tap to sync yout contacts</Text>
                        </TouchableOpacity>
                    </View>
                    
                    <AntDesign name="close" size={30} color="#D3D3D3" style={{ marginTop: 5, marginRight: 10 }}/>
                </View>

                <Text style={{ fontWeight: 'bold', fontSize: 18, alignSelf: 'flex-start', marginVertical: 15}}>Stories</Text>

                <View style={{ width: 350, flexDirection: 'row', paddingVertical: 5, borderRadius: 10, borderWidth: 0.2, justifyContent: 'space-between' }}>
                    <View style={{flexDirection: 'row'}}>
                        <Feather name="camera" size={30} color="#0EADFF" style={{ paddingLeft: 20 }}/>
                        <TouchableOpacity style={{ marginLeft: 20, justifyContent: 'center' }}>
                            <Text style={{fontSize: 20}}>Add to My Story</Text>
                        </TouchableOpacity>
                    </View> 
                    <MaterialCommunityIcons name="dots-horizontal" size={30} color="#D3D3D3" style={{ paddingRight: 20 }}/>
                </View>

                <View style={{ width: 350, flexDirection: 'row', paddingVertical: 5, borderRadius: 10, marginTop: 10, borderWidth: 0.2, justifyContent: 'space-between' }}>
                    <View style={{flexDirection: 'row'}}>
                        <Feather name="camera" size={30} color="#0EADFF" style={{ paddingLeft: 20 }}/>  
                        <TouchableOpacity style={{ marginLeft: 20, justifyContent: 'center' }}>
                            <Text style={{fontSize: 20}}>Add to Snap Map</Text>
                        </TouchableOpacity>
                    </View>
                    <MaterialCommunityIcons name="dots-horizontal" size={30} color="#D3D3D3" style={{ paddingRight: 20 }}/>
                </View>

                <Text style={{ fontWeight: 'bold', fontSize: 18, alignSelf: 'flex-start', marginVertical: 15}}>Friends</Text>

                <View style={{ width: 350, flexDirection: 'row', paddingVertical: 5, borderRadius: 10, borderWidth: 0.2, justifyContent: 'space-between' }}>
                    <View style={{flexDirection: 'row'}}>
                        <Feather name="user-plus" size={30} color="#000" style={{ paddingLeft: 20 }}/>
                        <TouchableOpacity style={{ marginLeft: 20, justifyContent: 'center' }}>
                            <Text style={{fontSize: 20}}>My Friends</Text>
                        </TouchableOpacity>
                    </View> 
                    <AntDesign name="right" size={30} color="#D3D3D3" style={{ paddingRight: 20 }}/>
                </View>

                <Button 
                    mode="contained"
                    style = {{width: 130, height: 45, borderRadius: 50, top: '5%'}}
                    onPress = {() => userLogout()}
                >
                    <Text style={{textAlign: 'center', fontWeight: 'bold', fontSize: 20, marginVertical: 8, color: '#FFFFFF'}}>LOGOUT</Text>
                </Button>
            </View>
        </SafeAreaView>
    );
}