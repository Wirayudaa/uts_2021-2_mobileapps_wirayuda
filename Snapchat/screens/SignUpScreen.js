import React, { useState } from 'react';
import { View, Text, TextInput, TouchableOpacity, ActivityIndicator, SafeAreaView } from 'react-native';
import { Button } from 'react-native-paper';
import styles from '../styles/styles';
import { firebase } from '../firebase/config';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

export default function SignUpScreen() {
    const [Email, setEmail] = useState('')
    const [Password, setPassword] = useState('')
    const [Loading, setLoading] = useState(false)

    if(Loading) {
        return(
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <ActivityIndicator size="large" color="#0EADFF" />
            </View>
        )
    }

    const userSignUp = async () => {
        setLoading(true)
        if( !Email || !Password ) {
            alert("Please fill all the fields")
        }
        const result = await firebase.auth().createUserWithEmailAndPassword(Email, Password)
        firestore().collection('Users').doc(result.user.uid).set({
            name: Username,
            email: result.user.email,
            uid: result.user.uid,
        })
        setLoading(false)

    }
    
    

    return(
        <SafeAreaView style={styles.container}>
            <Text style={{fontSize: 25, marginBottom: 10}}>Sign Up</Text>
            <Text style={{color: '#0EADFF', alignSelf: 'flex-start', marginLeft: 30}}>EMAIL</Text>
            <TextInput 
                style = {styles.input}
                value = {Email}
                onChangeText = {(text) => setEmail(text)}
            />
            <Text style={{color: '#0EADFF', alignSelf: 'flex-start', marginLeft: 30}}>PASSWORD</Text>
            <TextInput 
                style = {styles.input}
                value = {Password}
                onChangeText = {(text) => setPassword(text)}
                secureTextEntry = {true}
            />
            <Button 
                mode="contained"
                style = {{width: 130, height: 45, borderRadius: 50, top: '15%'}}
                disabled = { Email, Password ? false:true }
                onPress = {() => userSignUp()}
            >
                <Text style={{textAlign: 'center', fontWeight: 'bold', fontSize: 20, marginVertical: 8, color: '#FFFFFF'}}>Sign Up</Text>
            </Button>
        </SafeAreaView>
    );
}