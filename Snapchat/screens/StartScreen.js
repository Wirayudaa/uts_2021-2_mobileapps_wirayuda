import React from 'react';
import { View, Text, SafeAreaView, TouchableOpacity, Image } from 'react-native';
import styles from '../styles/styles';

export default function StartScreen({navigation}) {
    return(
        <SafeAreaView style={styles.startContainer}>
            <View style={{flex: 1, justifyContent: 'space-between', alignItems: 'center'}}>
                <Image source={require('../assets/snapchat-logo.png')} style={{width: 120, height: 120, top: '35%'}}></Image>
                <View style={{marginBottom: 20, flexDirection: 'row'}}>
                <TouchableOpacity style={{width: 130, height: 45, backgroundColor: '#FFFFFF', borderRadius: 50}} onPress={() => {navigation.navigate('Login')}}>
                        <Text style={{textAlign: 'center', fontWeight: 'bold', fontSize: 20, marginVertical: 8}}>Log In</Text>
                    </TouchableOpacity> 
                    <TouchableOpacity style={{width: 130, height: 45, backgroundColor: '#0EADFF', borderRadius: 50, marginHorizontal: 10}} onPress={() => {navigation.navigate('Sign Up')}}>
                        <Text style={{textAlign: 'center', fontWeight: 'bold', fontSize: 20, marginVertical: 8, color: '#FFFFFF'}}>Sign Up</Text>
                    </TouchableOpacity> 
                </View>
            </View>
        </SafeAreaView>
    );
}