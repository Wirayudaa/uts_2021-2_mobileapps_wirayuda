import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FFFFFF',
    },
    input: {
        padding: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#CCC',
        width: '90%',
        marginBottom: 10,
        borderRadius: 5,
        color: '#000'
    },
    btnText: {
        fontSize: 20,
        color: '#CC4F41',
    },
    startContainer: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#FFFC00',
    }
});

export default styles